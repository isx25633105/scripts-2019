#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Nombre de usuario y que lo busque en el sistema
# ------------------------------------------
ERR_ARG=1
ERR_NOEX=2
if [ $# -lt 1 ];then
	echo "ERROr: numero argumento incorrecto"
	echo "No has introducido nada $0"
	exit $ERR_ARG
fi
for nom in $*
do
  cut -d: -f1 /etc/passwd | grep "$nom" &> /dev/null 
  if [ $? -ne 0 ]; then
    echo "ERROR: $nom not in /etc/passwd" >> /dev/stderr
    echo "USAGE: user in /etc/passwd" >> /dev/stderr
  else 
    echo "El usuario $nom existe en el sistema"
  fi
done
exit 0
