#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Febrer 2020
# Processar els arguments i comptar quantes n̉hi ha de 3 o méés caràcters.
# ------------------------------------------
ERR_ARG=1
if [ $# -lt 1 ];then
	echo 'EROOR: arguments incorrectes'
	echo 'no has introduit un argument valid'
        exit $ERR_ARG
fi
count=0
for arg in $*
do
  cuenta=$(echo "$arg" | wc -c)
  if [ $cuenta -ge 3 ]; then
   count=$(($count+1))
  fi
done
echo $count
exit 0




