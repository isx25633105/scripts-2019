#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
# ------------------------------------------
ERR_ARG=1
if [ $# -lt 1 ];then
	echo 'EROOR: arguments incorrectes'
	echo 'no has introduit un argument valid'
        exit $ERR_ARG
fi
for arg in $*
do
  cuenta=$(echo "$arg" | wc -c)
  if [ $cuenta -ge 4  ]; then 
	  echo "$arg"
  fi
done





