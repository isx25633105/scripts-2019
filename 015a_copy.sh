#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# Copiar un fitxer a un altre directori
ERR_NARGS=1
ERR_NOREGULARFILE=2
ERR_NODIR=3

if [ $# -lt 2 ]; then
	echo "ERROR: num arg incorrecte"
	echo "Usage: $0  dir"
	exit $ERR_NARGS
fi

desti=$(echo $* | sed -r 's/^.* //')
llista=$(echo $* | sed -r 's/ [^ ]*$//')

if !  [ -d $desti ]; then 
	echo "ERROR: $2 no és un directori"
	echo "usage: $2 dir"
	exit $ERR_NODIR
fi

for files in $llista
do
  if ! [ -f $files ]; then
    echo "ERROR: $files no és un regular file"
    echo "usage: $files fitxer"
  fi
  $(cp -r $files $desti)
done



exit 0
