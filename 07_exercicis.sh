#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Processar linia a linia per la entrada estandart si la linea te més de 60 caracters mostra sino no
# ------------------------------------------
while read -r linea
do
  cuenta=$(echo "$linea" | wc -c)  
  if [ $cuenta -gt 60 ]; then
    echo "$linea"
   fi
done  
