#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# Validar que es rep un argument i que és un direcori i llistar-ne el contingut,
#per llistar el contingut amb un simple ls ja n'hi ha prou
ERR_NARGS=1
ERR_NODIR=2

if [ $# -ne 1 ]; then
	echo "ERROR: num arg incorrecte"
	echo "Usage: $0  dir"
exit $ERR_NARGS
fi

if !  [ -d $1 ]; then 
	echo "ERROR: $1 no és un directori"
	echo "usage: $0 dir"
exit $ERR_NODIR
fi
ls $dir
exit 0

