#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Mostrar linea a linea l'entrada estandart, retallant nomes les 50 primeres entrades
# ------------------------------------------

contador=0


while read -r line
do 
    echo "$line" | cut -c1-50
done   
    
exit 0



