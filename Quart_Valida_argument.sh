#!/bin/bash 
#ASIX isx25633190
#Febrer 2020
#2 Arguments: Valida synopisis, mostrar els arguments

#Argument 1
if [ $# -ne 2 ]; then 
	echo "ERROR: ·#args incorrectes"
	echo "usage: prog arg1 arg2 "
	exit 1
fi
#xixa
echo "Els dos arg son: $1, $2 "
exit 0
