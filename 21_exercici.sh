#!/bin/bash
for gid in $*
 do
   LiniaGid=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
   if [ $? -eq 0 ]; then
    gname=$(echo $LiniaGid | cut -d: -f1 | tr '[a-z]' '[A-Z]')
    userList=$(echo $LiniaGid | cut -d: -f4)
    echo "gname: $gname gid: $gid user: $userList"
   else 
     echo "ERROR no esta al sistema" >> /dev/stderr 
    fi    
 done
