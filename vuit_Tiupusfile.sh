#!/bin/bash
#ASIX1 isx25633105
#Febrer 2020
#Dir si un file esun regular file, un directori o un link.
#------------------
ERR_NARGS=1
if [ $# -ne 1 ]; then 
	echo "ERROR: #arg incorrecte"
	echo "usage: $0 no"
	exit $ERR_NARGS
fi

fit=$1
if ! [ -e $fit ]; then
	echo "$fit no existeix"
elif [ -h $fit ]; then
	echo "$fit Es un link simple"

elif [ -d $fit ]; then
	echo "$fit es un directori"
elif [ -f $fit ]; then
	echo "$fit es un fitxer regular"
else 
	echo "$fit es una cosa"
fi 
exit 0
