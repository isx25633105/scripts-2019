#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Mostrar linea a linea l'entrada estandart, retallant nomes les 50 primeres entrades
# ------------------------------------------
ERR_ARGS=1
if [ $# -lt 1 ]; then
    echo "ERROR: num arg incorrectes"
    echo "$0 No has introduit ningún dia de la setmana"
    exit $ERR_ARGS
fi

for dies in $*
do
   case $dies in
	   "dilluns"|"dimarts"|"dimecres"|"dillous"|"divendres")
		   laborables=$(($laborables+1));;
	   "dissabte"|"diumenge")
		   festius=$(($festius+1));;
	   *)
		   echo "dia: $dia erroni" >> /dev/stderr;;  
  esac
done
echo "Dies laborables:$laborables"
echo "Dies festius:$festius"
exit 0
		  
