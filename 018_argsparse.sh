#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# [-a-b-c-d-e-e] ang [...]
ERR_NARGS=1


if [ $# -eq 0 ]; then
	echo "ERROR: num arg incorrecte"
	echo "Usage: $0  [-a -b -c -d -e -f] arg[...]"
	exit $ERR_NARGS
fi


while [ -n "$1" ]; 
do
    if [ $1 -eq "-a" ]; then
      shift
      echo "-a $1"
    elif  [ $1 -eq "-d" ]; then
      shift
      echo "-d $1"
    fi	
done


for arg in $*
do
   case $arg in
   "-a"|"-b"|"-c"|"-d"|"-e"|"-f")
        opcions="$opcions $arg";;
	*)
        arguments="$arguments $arg";;
	*)
   esac
done

echo "Qpcions: $opcions"
echo "Arguments: $arguments"
exit 0

