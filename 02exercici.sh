#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# Mostar els arguments rebuts línia a línia, tot numerànt-los.
ERR_NARGS=1

if [ $# -lt 1 ]; then
    echo "ERROR: num arg incorrecte"
    echo "Usage: $0  arg"
    exit $ERR_NARGS
fi

contador=1
for arg in $*
do
    echo "$contador:$arg"
    contador=$((contador+1))
done

exit 0


