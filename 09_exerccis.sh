#! /bin/bash
#isx25633105ASIX M01-ISO Curs 2019-2020
#Nombre de usuario introducido por stdor y que lo busque en el sistema
#-----------------------------------------------------------

while read -r line
do
  grep "^$line:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: $line not in /etc/passwd" >> /dev/stderr
    echo "USAGE: user in /etc/passwd" >> /dev/stderr
  else
    echo "El usuario $line existe en el sistema" 
  fi
done

