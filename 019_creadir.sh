#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# noudir[..]
ERR_NARGS=1
ERR_MKDIR=2

if [ $# -lt 1 ]; then
	echo "ERROR: num argument incorrecte"
	echo "usage: $0 nomdir[...]"
        exit $ERR_NARGS
fi
for nom in $*
do
    mkdir $nom &> /dev/null 
    if [ $? -ne 0 ]; then
	    echo "Error: No crear $nom" >> /dev/stderr
	    stauts=$ERR_MKDIR
    fi
done
exit $status
