#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# Mostrar l'entrada estàndard numerant línea a línea
contador=1

while read -r line
do
    echo "$contador:$line"
    contador=$((contador+1))
    
done
exit 0

