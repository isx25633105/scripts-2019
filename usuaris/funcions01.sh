#! /bin/bash
# isx25633105 ASIX M01_ISO
# Març 2020
# funcions
#---------------

function hola(){
  echo "hola"
  return 0
}

function dia(){
  date
  return 0
}

function suma(){
  echo $(($1+$2))
  return 0
}
#showUser(login)
#mostrar un a un els camps amb la arrel
function showUser(){
#1) validar rep 1 arg
 if [ $# -ne 1 ]; then
   echo "ERROR: arguments incorrectes $0"
   echo "usage: $1"
   return 2
 fi
 login=$1
 line=""
 line=$(grep "^$login:" /etc/passwd 2> /dev/null)
 if [ -z "$line" ]; then
   echo "Error: login $login inexist"
   echo "usage: $0 login"
   return 3
 fi 
 
 gid=$(echo $line | cut -d: -f4)
 uid=$(echo $line | cut -d: -f3)
 home=$(echo $line | cut -d: -f6) 
 shell=$(echo $line | cut -d: -f7)
 echo "login: $login"
 echo "gid: $gid"
 echo "uid: $uid"
 
 echo "shell: $shell"
 return 0
} 

#showUsersGecos
#validar rep un argument
 #mostrar un a un els camps amb la arrel
function showUserGecos(){
 #1) validar rep 1 arg
 if [ $# -ne 1 ]; then
   echo "ERROR: arguments incorrectes $0"
   echo "usage: $1"
   return 2
 fi
 login=$1
 line=""
 line=$(grep "^$login:" /etc/passwd 2> /dev/null)
 if [ -z "$line" ]; then
   echo "Error: login $login inexist"
   echo "usage: $0 login"
   return 3
 fi
 login=$(echo $login)
 name=$(echo $line | cut -d: -f1)
 office=$(echo $gecos | cut -d: -f2)
 phoneOffi=$(echo $gecos | cut -d: -f3)
 phoneHome=$(echo $gecos | cut -d: -f4)
 echo "name: $name"
 echo "office: $office"
 echo "office phone: $phoneOffi"
 echo "office home: $phoneHome"
 return 0
}

function showGroup(){
  #1) validar rep 1 arg
 if [ $# -ne 1 ]; then
  echo "ERROR: arguments incorrectes $0"
  echo "usage: $1"
  return 2
 fi
 gname=$1
 line=""
 line=$(grep "^$gname:" /etc/group 2> /dev/null)
 if [ -z "$line" ]; then
  echo "Error: login $login inexist"
  echo "usage: $0 login"
  return 3
 fi
 x=$(echo $line | cut -d: -f2)
 ngrupo=$(echo $line | cut -d: -f3)

 echo "gname: $gname"
 echo "x: $x"
 echo "numero de grup: $ngrupo"
 echo "userlist: $userlist"
 return 0
}
function showUser(){
 #1) validar rep 1 arg
 if [ $# -ne 1 ]; then
  echo "ERROR: arguments incorrectes $0"
  echo "usage: $1"
  return 2
 fi
 login=$1
 line=""
 line=$(grep "^$login:" /etc/group 2> /dev/null)
 if [ -z "$line" ]; then
  echo "Error: login $gid inexist"
  echo "usage: $0 login"
  return 3
 fi
 x=$(echo $line | cut -d: -f2)
 uid=$(echo $line | cut -d: -f3)
 gid=$(echo $line | cut -d: -f4)
 Gecos=$(echo $line | cut -d: -f5)
 home=$(echo $line | cut -d: -f6)
 echo "login: $login"
 echo "gname: $gname"
 echo "x: $x"
 echo "numero de grup: $ngrupo"
 echo "userlist: $userlist"
 return 0
}
function showUserList(){
 #1) validar rep 1 arg
 if [ $# -lt 1 ]; then
  echo "ERROR: arguments incorrectes $0"
  echo "usage: $1"
  return 2
 fi
 for arg in $*
 do
  line=""
  line=$(grep "^$arg:" /etc/passwd 2> /dev/null)
  if [ -z "$line" ]; then
   echo "Error: login $arg inexist" 2> /dev/stdeer
   echo "usage: $0 login" 2> /dev/stdeer
   
  else
   gid=$(echo $line | cut -d: -f4)
   uid=$(echo $line | cut -d: -f3)
   home=$(echo $line | cut -d: -f6)
   shell=$(echo $line | cut -d: -f7)
   gecos=$(echo $line | cut -d: -f5)
   echo "login: $arg"
   echo "gid: $gid"
   echo "uid: $uid"
   echo "Gecos:$gecos"
   echo "shell: $shell"
  fi
 done
 return 0
} 

function showUserIn(){
 #1) validar rep 1 arg
 while read -r login
  do
   nom=""
   nom=$(grep "^$login:" /etc/passwd >> /dev/null)
   if [ -z "$nom" ]; then
    echo "Error: login $login inexist" >> /dev/stderr
    echo "usage: $0 login" >> /dev/stderr
    status=$(($status+1))
   
   else
    gid=$(echo $nom | cut -d: -f4)
    uid=$(echo $nom | cut -d: -f3)
    home=$(echo $nom | cut -d: -f6)
    shell=$(echo $nom | cut -d: -f7)
    gecos=$(echo $nom | cut -d: -f5)
    echo "login: $login"
    echo "gid: $gid" 
    echo "uid: $uid"
    echo "Gecos:$gecos"
   echo "shell: $shell"
   fi
  done                                             
 return 0
}
function showGroupMainMembers(){
  if [ $# -lt 1 ];then
   echo 
   echo 
   return 1
  fi 
	  
  
  nom=""
  nom=$(grep "^$gname:" /etc/group | cut -d: -f3 2> /dev/null)
  if [ -z "$nom" ]; then
   echo "Error: login $gname inexist" 2> /dev/stderr
   echo "usage: $0 login" 2> /dev/stderr
   return 2 
  fi
  echo "LListat del grup $gname($gid)"
  grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd  | cut -d: -f1,3,6,7 | sort -k2g,2 | sed 's/:/  /g' | tr '[a-z]' '[A-Z]' | sed -r 's/^(.*)$/\t\1/'
  echo "Aqui acaba el llistat"
}
function ShowAllShell(){
  llista=$(cut -d: -f7 /etc/passwd | sort -u)
  num=0
  for shell in $llista
   do 
    num=$(($num+1))
    wc=$(grep ":$shell$" /etc/passwd | wc -l)
    if [ $wc -ge 3 ]; then
     echo "$num: $shell($wc)"
     grep ":$shell$" /etc/passwd | cut -d: -f1,3,4,5 | sort -k3,3 | sed -r 's/^(.*)$/\t\1/' 
    fi
   done
   return 0
}
