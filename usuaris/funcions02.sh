#! /bin/bash
#isx25633105 ASIX1 M01_ISO
#Març 2020
#Funcions 2
#----------

function Numread(){
 num=0
 while read -r line
 do
  num=$(($num+1))
  echo "$num: $line"
 done
}
function numFile(){
 if [ $# -ne 1 ]; then
   echo "ERROR: Argument"
   echo "Usage: $0"
   exit 0
 fi
 filein=$1
 num=0
 if [ ! -f $filein ]; then
   echo "ERROR: $filein no es un fitxer"
   echo "usage: Un fitxer valid"
   exit 0
 else	 
   while read -r line
   do
     num=$(($num+1))
     echo "$num: $line"
   done < $filein
  fi
  return 0
}
function numFile(){		   
 num=0
 filein=/dev/stdin
 if [ $# -eq 1 ]; then
   if [ ! -f "$1" ]; then
     echo "ERROR: $1 no es un fitxer"
     echo "usage: Un fitxer valid" 
     return 2
   else
     filein=$1
   fi
 fi
 while read -r line
 do
   num=$(($num+1))
   echo "$num: $line"
 done < $filein  
 return 0
}
function filterGid(){
 filein=/dev/stdin
 if [ $# -eq 1 ]; then
  if [ ! -f $1 ]; then
   echo "ERROR $1 no es un fitxer"
   echo "usage; Argument"
   return 1
  else
    filein=$1
  fi
 fi
 while read -r line
 do 
  gid=$(echo "$line" | cut -d: -f4)
  if [ $gid -ge 500 ]; then
    echo "$line" | cut -d: -f1,3,4,6
  fi
 done < $filein
 return 0
}

