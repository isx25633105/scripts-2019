#!/bin/bash
if [ $# -lt 1 ]; then
  echo "ERROR args"
  echo "$0 no has introduit res"
  exit 1
fi

for uid in $*
 do
   LiniaUid=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
   if [ $? -eq 0 ]; then
    login=$(echo $LiniaUid | cut -d: -f1) 
    gid=$(echo $LiniaUid | cut -d: -f4)
    gname=$(egrep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1 | tr '[a-z]' '[A-Z]')
    home=$(echo $LiniaUid | cut -d: -f6)
    shell=$(echo $LiniaUid | cut -d: -f7)
    echo "login: $login gid: $gid gname= $gname home= $home shell= $shell"     
    else 
     echo "ERROR no esta $uid al sistema" >> /dev/stderr 
    fi    
 done
