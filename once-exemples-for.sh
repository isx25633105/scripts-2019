#!/bin/bash
#isx25633105 ASIX1
#Febrer 2020
#Descripció: exemples bucle for
#------------------
ERR_NARGS=1
ERR_ARGVL=2
OK=0



#llistar numerat
count=1
for arg in $*
do
	echo "$count: $arg"
	count=$((count+1))
done

exit 0

#iterar per la llista d'arguments
for arg in "$@"
do
	echo $arg
done
exit 0


#iterar per la llista d'arguments
for arg in "$*"
do
	echo $arg
done
exit 0



llistat=$(ls)
#Iterar per un contingut d'una variable
for nom in $llistat
do
	echo $nom
done
exit 0

#iterar per el contingut d'una cadena
#Només itera un cop
for nom in "pere pau marta anna"
do
	echo $nom
done
exit 0


for nom in "pere" "pau" "marta" "anna"
do
	echo $nom
done
exit 0
if [ $# -ne 1 ]; then
	echo "ERROR: #arg incorrecte"
	echo "usage: $0 no es res"
	exit $ERR_NARGS

fi
#2 validar si help
if [ "$1" = "-h" -o "$1" = "--help" ]; then
	echo "Escola del treball"
	echo "isx25633105 ASIX-M01"
	echo "usage: $0 mes"
	exit $OK

fi
#XIXA
mes=$1
if ! [ $mes -ge 1 -a $mes -le  12 ]; then
	echo "ERROR: #arg incorrecte"
	echo "usage: $mes no es un més valid"
	exit $ERR_ARGVL

fi
	

