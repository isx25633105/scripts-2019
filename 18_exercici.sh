#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Març 2020

# Programa: prog file…
# a)Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s̉ha comprimit correctament, o un missatge d̉error per stderror si no s̉ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit.
# Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
# b) Ampliar amb el cas: prog -h|--help.

# ----------------------------------------
status=1
if [ $# -lt 1 ]; then
  echo "ERROR: #arg incorrecte"
  echo "Usage: $0 no"
  exit $status
fi

status=0

if [ "$1" = "-h" -o "$1" = "--help" ]; then
  echo "Escola del treball"
  echo "isx25633105 ASIX-M01"
  echo "usage: $1 ayuda"
  exit $status
fi
if [ -f $* ]; then
  echo "Existeix un fitxer"
fi
cuenta=0
for arg in $*
do
  tar -cvzf $arg.tgz  $arg &> /dev/null
  if [ $? -ne 0 ]; then 
    echo "Error no s''ha comprimit" >> /dev/stderr
  else
    echo "$arg.tgz"
    cuenta=$(($cuenta+1))
  fi
done
echo "Número de comprimits $cuenta"
exit 0
