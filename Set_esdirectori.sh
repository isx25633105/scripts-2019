#!/bin/bash
#ASIX isx25633105
#Febrer 2020
#Programa que miri si es un directori i el llisti
if [ $# -ne 1 ]; then
	echo "ERROR: num arg incorrecte"
	echo "Usage: $0  dir"
	exit 1
fi

if !  [ -d $1 ]; then
	echo "ERROR: $1 no és un directori"
	echo "usage: $0 dir"
	exit 2
fi
dir=$1
ls $dir
exit 0
