#!/bin/bash
#ASIX isx25633105
#Febrer 2020
#Fer un comptador des de zero fins al valor indicat per l̉argument rebut.
ERR_NARGS=1

if [ $# -lt 1 ]; then
    echo "ERROR: num arg incorrecte"
    echo "Usage: $0  No es un numero"
    exit $ERR_NARGS
fi
contador=0
FINAL=$*
while [ $contador -ne $FINAL ]
do
   echo "$contador"
   contador=$(($contador+1))
done
echo "$FINAL"
exit 0


