#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.
# ------------------------------------------
ERR_ARG=1
if [ $# -lt 1 ];then
	echo 'EROOR: arguments incorrectes'
	echo 'no has introduit un numero valid'
        exit $ERR_ARG
fi
contador=1
FINAL=$*
while read -r line
do
  if [ $contador -le $FINAL ]; then
	  echo "$contador:$line"
	  contador=$(($contador+1)) 
	    
  fi
done




