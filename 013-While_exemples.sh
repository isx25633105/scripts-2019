#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# Descripcio: exemple bucle while
ERR_NARGS=1
ERR_ARGVL=2
num=1
#Mostra tot stdi en may
read -r line
while [ $line != "FI" ]
do
	echo "$num: $line" | tr '[a-z]' '[A-Z]' 
	num=$((num+1))
	read -r line
done 
exit 0



read -r line
while [ $line != "FI" ]
do
	echo $line
	read -r line

done
exit 0

#Numerar lineas introduides
num=1
while read -r line
do
	echo "$num $line"
	num=$((num+1))
done
exit 0








#6) mostrar la entrada estandard línia a línia
while read -r line
do
	echo $line
	
done
exit 0
#Mostrar els arguments ordenats
num=1
while [ -n "$1" ]
do
	echo "$num, $1, $#, $*"
	num=$((num+1))
	shift
done
exit
0











while [ -n "$1" ]
do
	echo "$1 $#: $*"
	shift
done 
exit 0



# Mostrar els arguments
while [ $# -gt 0 ]
do
	echo "$#: $*"
	shift
done 
exit 0





N=0
num=$1
# mostrar del [1-10]
while [ $num -ge  $N ]
do
	echo -n  "$num, "
	num=$((num-1))
done
exit 0

MAX=10
num=1
# mostrar del [1-10]
while [ $num -le  $MAX ] 
do
	echo "$num"
	num=$((num+1))
done
exit 0
