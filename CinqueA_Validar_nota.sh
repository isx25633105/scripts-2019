#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# Validar nota
#Argument 1
#Validar argument
if [ $# -ne 1 ]; then
	 echo "Error: #args incorrecte"
	echo "Usage: $0 nota" 
	exit 1
fi
#si nota no és [0-10] plegar
if ! [ $1 -ge 0 -a $1 -le 10 ]; then
	echo "ERROR: nota $1 no valida [0-10]"
	echo "Usage: $0 nota"
	exit 2
fi
#XIXA
nota=$1
if [ $nota -ge 5 ]; then 
	echo "Aprovat"

elif [ $nota -le 5 ]; then 
	echo "Suspès"
fi
exit 0

