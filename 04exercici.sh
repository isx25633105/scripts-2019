#!/bin/bash
#ASIX isx25633105
#Febrer 2020
#
ERR_NARGS=1
ERR_ARGVL=2
if [ $# -lt 1 ]; then
    echo "ERROR: num arg incorrecte"
    echo "Usage: $0  No es un numero"
    exit $ERR_NARGS
fi

#Si es un més
mes=$1
for mes in $*
do
  if ! [ $mes -ge 1 -a $mes -le  12 ]; then
    echo "ERROR: #arg incorrecte"
    echo "usage: $mes no es un més valid"
  else
    case $mes in
      "4"|"6"|"9"|"11")
        echo "tiene 30 dias";;
      "1"|"3"|"5"|"7"|"8"|"10"|"12")
        echo "tiene 31 dias";;
      "2")
        echo "Febrero tiene 28 dias"
    esac
  fi
done 

exit 0



