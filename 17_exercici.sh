#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Febrer 2020

# Programa: prog -f|-d arg1 arg2 arg3 arg4
# a) Valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
# Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
# Exemple: prog -f carta.txt a.txt /tmp fi.txt → retorna status 2.
# b) Ampliar amb el cas: prog -h|--help.

# ----------------------------------------
status=1
if [ $# -ne 5 ]; then
  echo "ERROR: #arg incorrecte"
  echo "Usage: $0 no"
  exit $status
fi


status=0
tipus=$1
if [ "$tipus" = "-h" -o "$tipus" = "--help" ]; then
  echo "Escola del treball"
  echo "isx25633105 ASIX-M01"
  echo "usage: $1 ayuda"
  exit $status
fi
if [ $tipus != "-f" -a $tipus != "-d" ]; then
	echo "Error  no has introduit argumens"
	echo "usage -f -d"
	exit $ERR_NARG
fi
shift
for arg in $*
do 
  if ![ $tipus $arg ]; then
    status=2
  fi
done
echo "status $status"

