#!/bin/bash
#isx25633105 ASIX1
#Febrer 2020
#Programa que et digui els dies del més
#------------------
ERR_NARGS=1
ERR_ARGVL=2
OK=0
if [ $# -ne 1 ]; then 
	echo "ERROR: #arg incorrecte"
	echo "usage: $0 no es res"
	exit $ERR_NARGS

fi
#2 validar si help
if [ "$1" = "-h" -o "$1" = "--help" ]; then
	echo "Escola del treball"
	echo "isx25633105 ASIX-M01"
	echo "usage: $0 mes"
	exit $OK
fi

#Si es un més
mes=$1
if ! [ $mes -ge 1 -a $mes -le  12 ]; then
	echo "ERROR: #arg incorrecte"
	echo "usage: $mes no es un més valid"
	exit $ERR_ARGVL
fi
#XIXA
case $1 in 
	"4"|"6"|"9"|"11")
		echo "tiene 30 dias";;
	"1"|"3"|"5"|"7"|"8"|"10"|"12")
		echo "tiene 31 dias";;
	"2")
		echo "Febrero tiene 28 dias"
esac

exit 0
