#! /bin/bash
# isx25633105ASIX M01-ISO Curs 2019-2020
# Febrer 2020
# Procesar matricules per argument
# ------------------------------------------
ERR_ARG=1
if [ $# -lt 1 ];then
	echo 'EROOR: arguments incorrectes'
	echo 'no has introduit un argument valid'
        exit $ERR_ARG
fi
count=0
for matri in $*
do
  echo "$matri" | egrep "^[0-9]{4}-[A-Z]{3}$" 2> /dev/null
  if [ $? -ne 0 ]; then
    echo "$matri no es una matricula valida" >> /dev/stderr
    count=$((count+1))	        
  
  fi
done
exit $count




