#!/bin/bash
#ASIX isx25633105
#Febrer 2020
# Validar nota: Aprovat, suspès, notable i exelent.
# Prog not[...]
ERR_NARGS=1
ERR_ARGVL=2
if [ $# -lt 1 ]; then
	echo "Error: #args incorrecte"
	echo "Usage: $0 nota"
	exit $ERR_NARGS
fi
#si nota no és [0-10] plegar

for nota  in $*
do
	echo $nota 
	if ! [ $nota -ge 0 -a $nota -le 10 ]; then
		echo "ERROR RECUPERABLE: nota $nota no valida [0-10]" >> /dev/stderr
		echo "Valors vàlids: 0 - 10" >> /dev/stderr

	elif [ $nota -lt 5 ]; then
	        echo "Suspès"

	elif [ $nota -lt 7 ]; then
		echo "Aprovat"
	
	elif [ $nota -lt 9 ]; then
		echo "Notable"

	else
		echo "Exelent"
				

fi
done
exit 0

